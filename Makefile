SHELL = /bin/sh

.SUFFIXES:
.SUFFIXES: .cpp .o

rwildcard=$(foreach d, $(wildcard $1*), $(call rwildcard, $d/, $2) $(filter $(subst *, %, $2), $d))
OBJECTS = $(call rwildcard, ./, *.o)

# Google Test:
GTEST_INCLUDE_DIR = /usr/include/gtest
GTEST_LINK_FLAGS = -L/usr/lib/ -pthread
# the libgtest.a library needs to be linked AFTER the respective object files
GTEST_LIBRARY = -lgtest

CXX := g++
RM := rm -f
DEBUGGINGFLAGS = -g

TESTINGFLAGS = -DENABLE_TESTS
MEASURINGFLAGS = -DMEASURE
PLOTFLAGS = -DPLOTS

CPPFLAGS = -isystem $(GTEST_INCLUDE_DIR)
CXXFLAGS = -Wall -Wextra


################ fibonacci main file
FIB_SOURCES = ./src/fibonacci.cpp
FIB_OBJS = fibonacci.o
FIB_EXECUTABLE = $(OBJS:.o=)


################ test files + recompilation of fibonacci main file
#### recompilation necessary because of changing DEFINES 

FIB_TEST_SOURCES = ./src/fibonacci_unittest.cpp
FIB_TEST_OBJS = fibonacci_unittest.o
FIB_TEST_MAIN_OBJS = t_fibonacci.o
FIB_TEST_EXECUTABLE = $(FIB_TEST_OBJS:.o=)


################ profiler files + recompilation of fibonacci main file
#### recompilation necessary because of changing DEFINES 

PROFILER_SOURCES = ./src/profiler.cpp
FIB_PROFILER_OBJS = fibProfiler.o

FIB_MEASURE_OBJS = m_fibonacci.o
FIB_MEASURE_EXECUTABLE = $(FIB_MEASURE_OBJS:.o=)


################ plot files + recompilation of fibonacci main file
#### recompilation necessary because of changing DEFINES 

PLOT_SOURCES = ./src/profilerPlots.cpp
FIB_PLOT_OBJS = fibProfilerPlots.o

FIB_PROFILER_PLOT_OBJS = p_profiler.o 
RE_FIB_PLOT_OBJS = p_fibonacci.o
FIB_PLOT_EXECUTABLE = $(RE_FIB_PLOT_OBJS:.o=)


################ sorting main file

SORTING_SOURCES = ./src/sorting.cpp
SORTING_OBJS = sorting.o
SORTING_EXECUTABLE = $(SORTING_OBJS:.o=)


################ test files + recompilation of sorting main file
#### recompilation necessary because of changing DEFINES 

SORTING_TEST_SOURCES = ./src/sorting_unittest.cpp
SORTING_TEST_OBJS = sorting_unittest.o
SORTING_TEST_MAIN_OBJS = t_sorting.o
SORTING_TEST_EXECUTABLE = $(SORTING_TEST_OBJS:.o=)


################ recompilation of sorting main file
#### recompilation necessary because of changing DEFINES 

SORTING_PROFILER_OBJS = sortingProfiler.o
SORTING_MEASURE_OBJS = m_sorting.o
SORTING_MEASURE_EXECUTABLE = $(SORTING_MEASURE_OBJS:.o=)

################ recompilation of sorting main file
#### recompilation necessary because of changing DEFINES 

SORTING_PLOT_OBJS = sortingProfilerPlots.o
RE_SORTING_PLOT_OBJS = p_sorting.o
SORTING_PLOT_EXECUTABLE = $(SORTING_PLOT_OBJS:.o=)


.PHONY: all
all: fibBuild

.PHONY: fibBuild
build: $(FIB_EXECUTABLE)

$(FIB_EXECUTABLE) : $(FIB_OBJS)
	$(CXX) $< -o $@

$(FIB_OBJS) : $(FIB_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@
	
.PHONY: fibTests
fibTests: $(FIB_TEST_EXECUTABLE)

$(FIB_TEST_EXECUTABLE) : $(FIB_TEST_OBJS) $(FIB_TEST_MAIN_OBJS)
	$(CXX) $(GTEST_LINK_FLAGS) $^ $(GTEST_LIBRARY) -o $@
	./$(FIB_TEST_EXECUTABLE)

$(FIB_TEST_OBJS) : $(FIB_TEST_SOURCES) 
	$(CXX) $(CPPFLAGS) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(FIB_TEST_MAIN_OBJS) : $(FIB_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(TESTINGFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: fibMeasure
fibMeasure: $(FIB_MEASURE_EXECUTABLE)

$(FIB_MEASURE_EXECUTABLE) : $(FIB_MEASURE_OBJS) $(FIB_PROFILER_OBJS)
	$(CXX) $(DEBUGGINGFLAGS) $^ -o $@
	./removeDat.sh
	./$(FIB_MEASURE_EXECUTABLE)

$(FIB_MEASURE_OBJS) : $(FIB_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(MEASURINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(FIB_PROFILER_OBJS) : $(PROFILER_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: fibPlots
fibPlots: $(FIB_PLOT_EXECUTABLE)

$(FIB_PLOT_EXECUTABLE) : $(FIB_PROFILER_PLOT_OBJS) $(FIB_PLOT_OBJS) $(RE_FIB_PLOT_OBJS) 
	$(CXX) $^ -o $@
	./$(FIB_PLOT_EXECUTABLE)

$(RE_FIB_PLOT_OBJS) : $(FIB_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(PLOTFLAGS) $(CXXFLAGS) -c $< -o $@

$(FIB_PLOT_OBJS) : $(PLOT_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(FIB_PROFILER_PLOT_OBJS) : $(PROFILER_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@	

.PHONY: sortTest
sortTest: $(SORTING_TEST_EXECUTABLE)

$(SORTING_TEST_EXECUTABLE) : $(SORTING_TEST_OBJS) $(SORTING_TEST_MAIN_OBJS)
	$(CXX) $(GTEST_LINK_FLAGS) $^ $(GTEST_LIBRARY) -o $@
	./$(SORTING_TEST_EXECUTABLE)	

$(SORTING_TEST_OBJS) : $(SORTING_TEST_SOURCES) 
	$(CXX) $(CPPFLAGS) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(SORTING_TEST_MAIN_OBJS) : $(SORTING_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(TESTINGFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: sortMeasure
sortMeasure: $(SORTING_MEASURE_EXECUTABLE)

$(SORTING_MEASURE_EXECUTABLE) : $(SORTING_MEASURE_OBJS) $(SORTING_PROFILER_OBJS)
	$(CXX) $(DEBUGGINGFLAGS) $^ -o $@
	./removeDat.sh
	./$(SORTING_MEASURE_EXECUTABLE)

$(SORTING_MEASURE_OBJS) : $(SORTING_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(MEASURINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(SORTING_PROFILER_OBJS) : $(PROFILER_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: sortPlots
sortPlots: $(SORTING_PLOT_EXECUTABLE)

$(SORTING_PLOT_EXECUTABLE) : $(SORTING_PROFILER_PLOT_OBJS) $(SORTING_PLOT_OBJS) $(RE_SORTING_PLOT_OBJS) 
	$(CXX) $^ -o $@
	./$(SORTING_PLOT_EXECUTABLE)

$(SORTING_PLOT_OBJS) : $(SORTING_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(PLOTFLAGS) $(CXXFLAGS) -c $< -o $@

$(RE_SORTING_PLOT_OBJS) : $(PLOT_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@

$(SORTING_PROFILER_PLOT_OBJS) : $(PROFILER_SOURCES)
	$(CXX) $(DEBUGGINGFLAGS) $(CXXFLAGS) -c $< -o $@	


.PHONY: clean
clean:
	$(RM) $(OBJECTS) $(FIB_EXECUTABLE) $(FIB_TEST_EXECUTABLE) $(FIB_MEASURE_EXECUTABLE) $(FIB_PLOT_EXECUTABLE) $(SORTING_EXECUTABLE) $(SORTING_TEST_EXECUTABLE) $(SORTING_MEASURE_EXECUTABLE) $(SORTING_PLOT_EXECUTABLE)
