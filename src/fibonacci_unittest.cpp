#include <gtest/gtest.h>
#include "fibonacci.hpp"


int main(int argc, char **argv){

	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

/*
3:
TEST functions test the functions from fibonacci.cpp for correctness
*/

TEST(fibonacciFromDefinition, PositiveNos){
	ASSERT_TRUE(fibonacciFromDefinition(0) == 0);
	ASSERT_TRUE(fibonacciFromDefinition(1) == 1);
	ASSERT_TRUE(fibonacciFromDefinition(2) == 1);
	ASSERT_TRUE(fibonacciFromDefinition(3) == 2);
	ASSERT_TRUE(fibonacciFromDefinition(4) == 3);
	ASSERT_TRUE(fibonacciFromDefinition(5) == 5);
	ASSERT_TRUE(fibonacciFromDefinition(6) == 8);
	ASSERT_TRUE(fibonacciFromDefinition(7) == 13);
	}

TEST(fibonacciLinear, PositiveNos){
	ASSERT_TRUE(fibonacciLinear(0) == 0);
	ASSERT_TRUE(fibonacciLinear(1) == 1);
	ASSERT_TRUE(fibonacciLinear(2) == 1);
	ASSERT_TRUE(fibonacciLinear(3) == 2);
	ASSERT_TRUE(fibonacciLinear(4) == 3);
	ASSERT_TRUE(fibonacciLinear(5) == 5);
	ASSERT_TRUE(fibonacciLinear(6) == 8);
	ASSERT_TRUE(fibonacciLinear(7) == 13);
	ASSERT_TRUE(fibonacciLinear(30) == 832040);
}

TEST(fibonacciConstant, PositiveNos){
	ASSERT_TRUE(fibonacciConstant(0) == 0);
	ASSERT_TRUE(fibonacciConstant(1) == 1);
	ASSERT_TRUE(fibonacciConstant(2) == 1);
	ASSERT_TRUE(fibonacciConstant(3) == 2);
	ASSERT_TRUE(fibonacciConstant(4) == 3);
	ASSERT_TRUE(fibonacciConstant(5) == 5);
	ASSERT_TRUE(fibonacciConstant(6) == 8);
	ASSERT_TRUE(fibonacciConstant(7) == 13);
}

TEST(fibonacciMatrix, PositiveNos){
	ASSERT_TRUE(fibonacciMatrix(0) == 0);
	ASSERT_TRUE(fibonacciMatrix(1) == 1);
	ASSERT_TRUE(fibonacciMatrix(2) == 1);
	ASSERT_TRUE(fibonacciMatrix(3) == 2);
	ASSERT_TRUE(fibonacciMatrix(4) == 3);
	ASSERT_TRUE(fibonacciMatrix(5) == 5);
	ASSERT_TRUE(fibonacciMatrix(6) == 8);
	ASSERT_TRUE(fibonacciMatrix(7) == 13);
	ASSERT_TRUE(fibonacciMatrix(30) == 832040);	
}

/* 
The test fails due to the input parameter n being of type UNSIGNED int
TEST(fibonacciMatrix, negativeNumber){
	ASSERT_EQ(fibonacciMatrix(-1), -1)
}*/

TEST(fibonacciFormula, PositiveNos){
	ASSERT_TRUE(fibonacciFormula(0) == 0);
	ASSERT_TRUE(fibonacciFormula(1) == 1);
	ASSERT_TRUE(fibonacciFormula(2) == 1);
	ASSERT_TRUE(fibonacciFormula(3) == 2);
	ASSERT_TRUE(fibonacciFormula(4) == 3);
	ASSERT_TRUE(fibonacciFormula(5) == 5);
	ASSERT_TRUE(fibonacciFormula(6) == 8);
	ASSERT_TRUE(fibonacciFormula(7) == 13);
}
TEST(fibonacciFormula, bigNumber){
	ASSERT_EQ(fibonacciFormula(50), 12586269025);
	ASSERT_EQ(fibonacciLinear(70), fibonacciFormula(70));
}


/*
TEST(fibonacciLookup, PositiveNos){
	ASSERT_TRUE(Fib<0>::val == 0);
	ASSERT_TRUE(Fib<1>::val == 1);
	ASSERT_TRUE(Fib<2>::val == 1);
	ASSERT_TRUE(Fib<3>::val == 2);
	ASSERT_TRUE(Fib<4>::val == 3);
	ASSERT_TRUE(Fib<5>::val == 5);
	ASSERT_TRUE(Fib<6>::val == 8);
	ASSERT_TRUE(Fib<7>::val == 13);
}*/

TEST(fibonacciLookup, PositiveNos){
	ASSERT_TRUE(fibonacciLookup(0) == 0);
	ASSERT_TRUE(fibonacciLookup(1) == 1);
	ASSERT_TRUE(fibonacciLookup(2) == 1);
	ASSERT_TRUE(fibonacciLookup(3) == 2);
	ASSERT_TRUE(fibonacciLookup(4) == 3);
	ASSERT_TRUE(fibonacciLookup(5) == 5);
	ASSERT_TRUE(fibonacciLookup(6) == 8);
	ASSERT_TRUE(fibonacciLookup(7) == 13);
}
/* Test fails due to the lookup table only containing the first 15 Fibonacci numbers.
TEST(fibonacciLookup, OutOfBoundTest){
	ASSERT_EQ(fibonacciLookup(30), 832040);
}*/
