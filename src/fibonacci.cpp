/*
Exercises implementing fibonacci algorithms in the course "Algorithm Engineering" by J.K. Mueller at Friedrich Schiller University Jena
submitted by Hendrik Schmidt
*/

#include "fibonacci.hpp"

#ifdef MEASURE 
#include "profiler.hpp"

// Number of measurements used for statistics
#define MEASUREMENTS_REP 6
// Maximum Fibonacci number to be computed
#define MAX_FIB_NUM 80
#endif

#ifdef PLOTS
#include "profilerPlots.hpp"
#endif

using namespace std;

// passing -D{NAME} to g++ defines {NAME} with value 1

#ifndef ENABLE_TESTS 
int main(){

	#if defined(MEASURE) || defined(PLOTS) 

	string tmp_listfc[] = {"clockMeasurements_formula.dat", "clockMeasurements_matrix.dat", \
												"clockMeasurements_linear.dat","clockMeasurements_constant.dat",\
												"clockMeasurements_lookup.dat",	"clockMeasurements_fromDef.dat"};

	vector<string> filenamesClock(tmp_listfc, tmp_listfc + 6); 
	string tmp_listfcy[] = {"cycleMeasurements_formula.dat", "cycleMeasurements_matrix.dat", \
												"cycleMeasurements_linear.dat","cycleMeasurements_constant.dat",\
												"cycleMeasurements_lookup.dat",	"cycleMeasurements_fromDef.dat"};

	vector<string> filenamesCycle(tmp_listfcy, tmp_listfcy + 6);

	#endif

	#ifdef MEASURE

	profiler pFormula, pMatrix, pLinear, pConstant, pLookup, pDef;

	for(int n = 0; n < MAX_FIB_NUM; n++){

		for (int i = 0; i < MEASUREMENTS_REP; i++){
			pFormula.startClockMeasure();
			pFormula.startCycle = pFormula.cycleMeasure();

			fibonacciFormula(n);
	
			pFormula.stopCycle = pFormula.cycleMeasure();
			pFormula.stopClockMeasure();
			pFormula.clockMeasurements.push_back(pFormula.diffClock);
			pFormula.cycleMeasurements.push_back(pFormula.stopCycle - pFormula.startCycle);

			pMatrix.startClockMeasure();
			pMatrix.startCycle = pMatrix.cycleMeasure();

			fibonacciMatrix(n);

			pMatrix.stopCycle = pMatrix.cycleMeasure();
			pMatrix.stopClockMeasure();
			pMatrix.clockMeasurements.push_back(pMatrix.diffClock);
			pMatrix.cycleMeasurements.push_back(pMatrix.stopCycle - pMatrix.startCycle);

			pLinear.startClockMeasure();
			pLinear.startCycle = pLinear.cycleMeasure();

			fibonacciLinear(n);

			pLinear.stopCycle = pLinear.cycleMeasure();
			pLinear.stopClockMeasure();
			pLinear.clockMeasurements.push_back(pLinear.diffClock);
			pLinear.cycleMeasurements.push_back(pLinear.stopCycle - pLinear.startCycle);

			pConstant.startClockMeasure();
			pConstant.startCycle = pConstant.cycleMeasure();

			fibonacciConstant(n);

			pConstant.stopCycle = pConstant.cycleMeasure();
			pConstant.stopClockMeasure();
			pConstant.clockMeasurements.push_back(pConstant.diffClock);
			pConstant.cycleMeasurements.push_back(pConstant.stopCycle - pConstant.startCycle);

			if (n < 20){
				pDef.startClockMeasure();
				pDef.startCycle = pDef.cycleMeasure();

				fibonacciFromDefinition(n);

				pDef.stopCycle = pDef.cycleMeasure();
				pDef.stopClockMeasure();
				pDef.clockMeasurements.push_back(pDef.diffClock);
				pDef.cycleMeasurements.push_back(pDef.stopCycle - pDef.startCycle);
			}

			if (n <= 15){
				pLookup.startClockMeasure();
				pLookup.startCycle = pLookup.cycleMeasure();

				fibonacciLookup(n);

				pLookup.stopCycle = pLookup.cycleMeasure();
				pLookup.stopClockMeasure();
				pLookup.clockMeasurements.push_back(pLookup.diffClock);
				pLookup.cycleMeasurements.push_back(pLookup.stopCycle - pLookup.startCycle);
			}
		}
		pFormula = pFormula.doStats(pFormula);
		pFormula.writeToFile(pFormula, n, filenamesClock[0], 1);
		pFormula.writeToFile(pFormula, n, filenamesCycle[0], 2);
		pFormula.clockMeasurements.clear();
		pFormula.cycleMeasurements.clear();

		pMatrix = pMatrix.doStats(pMatrix);
		pMatrix.writeToFile(pMatrix, n, filenamesClock[1], 1);
		pMatrix.writeToFile(pMatrix, n, filenamesCycle[1], 2);
		pMatrix.clockMeasurements.clear();
		pMatrix.cycleMeasurements.clear();

		pLinear = pLinear.doStats(pLinear);
		pLinear.writeToFile(pLinear, n, filenamesClock[2], 1);
		pLinear.writeToFile(pLinear, n, filenamesCycle[2], 2);
		pLinear.clockMeasurements.clear();
		pLinear.cycleMeasurements.clear();

		pConstant = pConstant.doStats(pConstant);
		pConstant.writeToFile(pConstant, n, filenamesClock[3], 1);
		pConstant.writeToFile(pConstant, n, filenamesCycle[3], 2);
		pConstant.clockMeasurements.clear();
		pConstant.cycleMeasurements.clear();

		if (n < 20){
			pDef = pDef.doStats(pDef);
			pDef.writeToFile(pDef, n, filenamesClock[5], 1);
			pDef.writeToFile(pDef, n, filenamesCycle[5], 2);
			pDef.clockMeasurements.clear();
			pDef.cycleMeasurements.clear();
		}

		if (n <= 15){
			pLookup = pLookup.doStats(pLookup);
			pLookup.writeToFile(pLookup, n, filenamesClock[4], 1);
			pLookup.writeToFile(pLookup, n, filenamesCycle[4], 2);
			pLookup.clockMeasurements.clear();
			pLookup.cycleMeasurements.clear();
		}

	}

	#endif

	#ifdef PLOTS
	makePlots(filenamesClock, filenamesCycle, 0);
	#endif

	#if !defined(MEASURE) && !defined(PLOTS) 

	cout << fibonacciMatrix(25) << "moeeep" << endl;
	return 0;

	#endif
}
#endif

/*
2.1:
Fibonacci algorithm derived from the definition
*/

unsigned long fibonacciFromDefinition(unsigned int n){
	if (n < 2){
		return n;
	}else{
		return fibonacciFromDefinition(n - 1) + fibonacciFromDefinition(n - 2);
	}
}

/*
2.2:
Fibonacci algorithm using linear memory and running time in n
*/

unsigned long fibonacciLinear(unsigned int n){

	if (n == 0) return 0;
	if (n == 1) return 1;
	double arr[n+1];
	arr[0] = 0;
	arr[1] = 1;  

	for (unsigned int i = 2; i <= n; i++){
		arr[i] = arr[i-1] + arr[i-2];
	}
	return arr[n];
}

/*
2.3:
Fibonacci algorithm using constant memory and linear running time in n
*/

unsigned long fibonacciConstant(unsigned int n){
	double n_minus2 = 0, n_minus1 = 1;
	
	if (n < 2) return n;

	for (unsigned int i = 2; i <= n; i++){
		n_minus1 += n_minus2;
		n_minus2 = n_minus1 - n_minus2;
	}
	return n_minus1;
}

// Method for printing a matrix to standard out

void printm(vector<vector<unsigned long> > m){
	for (unsigned int i = 0; i < m.size(); i++){
		for (unsigned int j = 0; j < m[i].size(); j++){
			cout << m[i][j] << " ";
		}
		cout << endl;
	}
}

// Matrix multiplication method using a naive approach

vector<vector<unsigned long> > matrixMult(vector<vector<unsigned long> > m, vector<vector<unsigned long> > m2){
	
	int mRows = m[0].size(), mCols = m.size();
	int m2Rows = m2[0].size(), m2Cols = m2.size();

	//cout << "M:\n" << mRows << " x " << mCols << "\nM2:\n" << m2Rows << " x " << m2Cols << endl;

	if(mCols != m2Rows){
		cout << "\n" << "size of mCols: " << mCols << endl << "size of m2Rows: " << m2Rows << endl;
		cout << "The matrices to be multiplied are incompatible!\nCols of first need to be equal to rows of second!\n";
		exit(EXIT_FAILURE);
	}
	
	vector<vector<unsigned long> > res(mRows, vector<unsigned long>(m2Cols, 0.0));
	
	for (int i = 0; i < mRows; i++){
		for (int j = 0; j < m2Cols; j++){ 
			for (int z = 0; z < mCols; z++){
				res[i][j] += m[z][i] * m2[j][z];
			}
		}
	}
	return res;
}

// recursive function to perform exponentiation by squaring

vector<vector<unsigned long> > expBySquaring(vector<vector<unsigned long> > x, unsigned int n){

	// if 0 return identity matrix
	if (n == 0){
		vector<vector<unsigned long> > z(2, vector<unsigned long>(2, 0.0));
		z[0][0] = 1.0; 
		z[1][1] = 1.0;
		return z;
	} 
	if (n == 1) return x;

	vector<vector<unsigned long> > square = matrixMult(x, x);

	if (n % 2 == 0) return expBySquaring(square, n/2);

	// no if statement needed when all other cases have been covered.
	return matrixMult(x, expBySquaring(square, (n-1)/2));

}

// function to find the nth fibonacci number through exponentiation by squaring

unsigned long fibonacciMatrix(unsigned int n){

	vector<vector<unsigned long> > x(2, vector<unsigned long>(2, 1.0));
	x[0][0] = 0.0;
	
	return expBySquaring(x, n)[1][0];
}

// calculating the nth fibonacci number using a formula

unsigned long fibonacciFormula(unsigned int n){
	return floor(1/sqrt(5) * pow((1 + sqrt(5))/2, n) + 0.5);	
}

// implementation of a lookup table to be able to retrieve the first 15 fibonacci numbers
// numbers are calculated during compile time

long fibonacciLookup(unsigned int n){
	if(n > 15){
		cout << "ERROR: The largest fibonacci number to request is the 15th!" << endl << endl;
		exit(EXIT_FAILURE);
	}
	long lookupTable[15] = {Fib<0>::val, Fib<1>::val, Fib<2>::val, Fib<3>::val, 
		Fib<4>::val, Fib<5>::val, Fib<6>::val, Fib<7>::val, Fib<8>::val, Fib<9>::val, 
		Fib<10>::val, Fib<11>::val, Fib<12>::val, Fib<13>::val, Fib<14>::val};
	return lookupTable[n];
}
