#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "sorting.hpp"

#ifdef MEASURE
#include "profiler.hpp"

// Number of measurements used for statistics
#define MEASUREMENTS_REP 6
// Maximum input length of the arrays to be sorted
#define MAX_INPUT_LENGTH 1000

#endif

#ifdef PLOTS
#include "profilerPlots.hpp"
#endif

using namespace std;

#ifndef ENABLE_TESTS
int main(){

	#if defined(MEASURE) || defined(PLOTS)

	string tmp_listsc[] = {"clockMeasurements_insertionSort.dat", "clockMeasurements_quickSort.dat", \
												"clockMeasurements_mergeSort.dat", "clockMeasurements_cppSort.dat"};

	vector<string> filenamesClock(tmp_listsc, tmp_listsc + 4); 
	string tmp_listscy[] = {"cycleMeasurements_insertionSort.dat", "cycleMeasurements_quickSort.dat", \
												"cycleMeasurements_mergeSort.dat", "cycleMeasurements_cppSort.dat"};

	vector<string> filenamesCycle(tmp_listscy, tmp_listscy + 4);

	#endif

	#ifdef MEASURE

	profiler pInsertionSort, pQuickSort, pMergeSort, pCppSort;

	for(size_t n = 0; n < MAX_INPUT_LENGTH; n++){
		for (int i = 0; i < MEASUREMENTS_REP; i++){
			vector<int> z = generateTestInput(n, n, 0);
			pInsertionSort.startClockMeasure();
			pInsertionSort.startCycle = pInsertionSort.cycleMeasure();

			insertionSort(z);
	
			pInsertionSort.stopCycle = pInsertionSort.cycleMeasure();
			pInsertionSort.stopClockMeasure();
			pInsertionSort.clockMeasurements.push_back(pInsertionSort.diffClock);
			pInsertionSort.cycleMeasurements.push_back(pInsertionSort.stopCycle - pInsertionSort.startCycle);

			vector<int> zz = generateTestInput(n, n, 0);
			pQuickSort.startClockMeasure();
			pQuickSort.startCycle = pQuickSort.cycleMeasure();

			quickSort(zz, 0, (int)n);
	
			pQuickSort.stopCycle = pQuickSort.cycleMeasure();
			pQuickSort.stopClockMeasure();
			pQuickSort.clockMeasurements.push_back(pQuickSort.diffClock);
			pQuickSort.cycleMeasurements.push_back(pQuickSort.stopCycle - pQuickSort.startCycle);

			vector<int> zzz = generateTestInput(n, n, 0);
			pMergeSort.startClockMeasure();
			pMergeSort.startCycle = pMergeSort.cycleMeasure();

			mergeSort(zzz);
	
			pMergeSort.stopCycle = pMergeSort.cycleMeasure();
			pMergeSort.stopClockMeasure();
			pMergeSort.clockMeasurements.push_back(pMergeSort.diffClock);
			pMergeSort.cycleMeasurements.push_back(pMergeSort.stopCycle - pMergeSort.startCycle);

			vector<int> v = generateTestInput(n, n, 0);
			pCppSort.startClockMeasure();
			pCppSort.startCycle = pCppSort.cycleMeasure();

			std::sort(v.begin(), v.end());

			pCppSort.stopCycle = pCppSort.cycleMeasure();
			pCppSort.stopClockMeasure();
			pCppSort.clockMeasurements.push_back(pCppSort.diffClock);
			pCppSort.cycleMeasurements.push_back(pCppSort.stopCycle - pCppSort.startCycle);

		}
		pInsertionSort = pInsertionSort.doStats(pInsertionSort);
		pInsertionSort.writeToFile(pInsertionSort, n, filenamesClock[0], 1);
		pInsertionSort.writeToFile(pInsertionSort, n, filenamesCycle[0], 2);
		pInsertionSort.clockMeasurements.clear();
		pInsertionSort.cycleMeasurements.clear();

		pQuickSort = pQuickSort.doStats(pQuickSort);
		pQuickSort.writeToFile(pQuickSort, n, filenamesClock[1], 1);
		pQuickSort.writeToFile(pQuickSort, n, filenamesCycle[1], 2);
		pQuickSort.clockMeasurements.clear();
		pQuickSort.cycleMeasurements.clear();

		pMergeSort = pMergeSort.doStats(pMergeSort);
		pMergeSort.writeToFile(pMergeSort, n, filenamesClock[2], 1);
		pMergeSort.writeToFile(pMergeSort, n, filenamesCycle[2], 2);
		pMergeSort.clockMeasurements.clear();
		pMergeSort.cycleMeasurements.clear();

		pCppSort = pCppSort.doStats(pCppSort);
		pCppSort.writeToFile(pCppSort, n, filenamesClock[3], 1);
		pCppSort.writeToFile(pCppSort, n, filenamesCycle[3], 2);
		pCppSort.clockMeasurements.clear();
		pCppSort.cycleMeasurements.clear();

	}
	#endif

	#ifdef PLOTS
	makePlots(filenamesClock, filenamesCycle, 1);
	#endif

	#if !defined(MEASURE) && !defined(PLOTS) 

	return 0;

	#endif
}
#endif

/*
@brief: insertion sort algorithm
@params: vector to sort
@return: sorted vector
@complexity: Worst case: O(n²), Best case: O(n)
*/

template <typename T>
vector<T> insertionSort(vector<T> a){
	if (a.size() <= 1) return a;

	int j;
	for (size_t i = 1; i < a.size(); i++){
		T valueToSort = a[i];

		for (j = i - 1; (j >= 0) && (a[j] > valueToSort); j--){
			a[j+1] = a[j];
		}
		a[j+1] = valueToSort;
	}
	return a;
}

// instatiation of function template in the source file for type int
// alternative would be to define the template function in the header, 
// which is in my opinion not as clean as this variant
template vector<int> insertionSort(vector<int> a);

/*
@brief: in-place version of the quicksort algorithm
@params: input vector, first and last index of sequence to sort
@return: no return value, inputArr is altered in-place
@complexitiy: Worst case: O(n²), Best case: O(nlog(n)) 
*/

template <typename T>
void quickSort(vector<T>& inputArr, T first, T last){
	if (inputArr.size() > 1){
		T part;
		if(first < last){
			part = partition(inputArr, first, last);
			quickSort(inputArr, first, part);
			quickSort(inputArr, part + 1, last);
		}
	}
}
 
template void quickSort(vector<int>& inputArr, int first, int last);

template <typename T>
T partition(vector<T>& inputArr, T f, T l){
	
	T x = inputArr[f];
	T i = f;
	T j;
 
	for(j = f + 1; j < l; j++){
		if(inputArr[j] <= x){
			i++;
			swap(inputArr[i], inputArr[j]);
		}
	}
 
	swap(inputArr[i],inputArr[f]);
	return i;
}

template int partition(vector<int>& inputArr, int f, int l);


template <typename T>
void mergeSort2(vector<T>& inputArr, size_t leftIdx, size_t rightIdx){
	if (leftIdx < rightIdx){
		size_t mid = (size_t)(leftIdx + rightIdx) / 2;
		mergeSort2(inputArr, leftIdx, mid);
		mergeSort2(inputArr, mid + 1, rightIdx);

		merge2(inputArr, leftIdx, rightIdx);
	}
}

template <typename T>
void merge2(vector<T>& inputArr, size_t leftIdx, size_t rightIdx){
	size_t rightIdxInit = rightIdx;

	while (leftIdx != rightIdx){
		if (inputArr[leftIdx] > inputArr[rightIdx]){
			swap(inputArr[leftIdx], inputArr[rightIdx]);

		}
	}

}

/* 
@brief: recursive merge sort
@params: array to sort
@return: returns the sorted vector
@complexity:(Worst and Best case) O(nlog(n))
*/

template <typename T>
vector<T> mergeSort(vector<T>& inputArr){
	if (inputArr.size() <= 1) return inputArr;

	vector<T> left, right;

	size_t mid = (inputArr.size() + 1) / 2;

	for (size_t i = 0; i < mid; i++){
		left.push_back(inputArr[i]);
	}

	for (size_t j = mid; j < inputArr.size(); j++){
		right.push_back(inputArr[j]);
	}

	left = mergeSort(left);
	right = mergeSort(right);

	
	typename vector<T>::iterator it;
	it = copy(left.begin(), left.end(), inputArr.begin());
	copy(right.begin(), right.end(), it);

	std::inplace_merge(inputArr.begin(), it, inputArr.end());
	

	return inputArr;
	//return merge(inputArr, left, right);
}

template vector<int> mergeSort<int>(vector<int>& inputArr);

template <typename T>
vector<T> merge(vector<T>& inputArr, vector<T> l, vector<T> r){

	vector<T> res;
	unsigned int leftIdx = 0, rightIdx = 0;

	while(leftIdx < l.size() && rightIdx < r.size()){
		if (l[leftIdx] < r[rightIdx]){
			res.push_back(l[leftIdx]);
			leftIdx++;
		}else{
			res.push_back(r[rightIdx]);
			rightIdx++;
		}
	}

	while(leftIdx < l.size()){
		res.push_back(l[leftIdx]);
		leftIdx++;
	}

	while(rightIdx < r.size()){
		res.push_back(r[rightIdx]);
		rightIdx++;
	}
	inputArr = res;
	return inputArr;
}

template vector<int> merge<int>(vector<int>& inputArr, vector<int> l, vector<int> r);

/*
@brief: generate random test input with (or without) repeated elements
@params: n is the size of the input vector to be generated, m is the value after which 
				elements are repeated and seed is the seed for srand
@return: returns random vector of length n containing elements up to the value of m
@complexity: O(n)
*/

template <typename T>
vector<T> generateTestInput(size_t n, size_t m, T seed){
	
	vector<T> inputArr;
	srand(seed);

	for (size_t i = 0; i < n; i++){
		inputArr.push_back(rand() %m + 1);
	}

	return inputArr;	
}

template vector<int> generateTestInput(size_t n, size_t m, int seed);
