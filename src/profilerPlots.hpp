#include <vector>
#include <cstdio>
#include <cstdlib>
#include <fstream>

#ifndef PROFILERPLOTS_HPP
#define PROFILERPLOTS_HPP

void makePlots(std::vector<std::string> filenamesClock, std::vector<std::string> filenamesCycle, int whatToPlot);

#endif