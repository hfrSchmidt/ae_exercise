#include <gtest/gtest.h>
#include "sorting.hpp"


int main(int argc, char **argv){

	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


TEST(insertionSort, selfDefindedInput){

	int n = 7;
	std::vector<int> test(n, 2);
	test[4] = 3;
	test[3] = 7;
	test[2] = 6;
	test[5] = 1;
	test[1] = 5;
	test[6] = 4;

	std::vector<int> testSorted;
	for (int i = 0; i < n; i++) testSorted.push_back(i+1);

	ASSERT_EQ(insertionSort(test), testSorted);
}

TEST(insertionSort, emptyArray){
	std::vector<int> v;

	ASSERT_EQ(insertionSort(v), v);
}

TEST(quickSort, selfDefindedInput){
	int n = 7;
	std::vector<int> test(n, 2);
	test[4] = 3;
	test[3] = 7;
	test[2] = 6;
	test[5] = 1;
	test[1] = 5;
	test[6] = 4;
	
	std::vector<int> testSorted;
	for (int i = 0; i < n; i++) testSorted.push_back(i+1);

	quickSort(test, 0, n);

	ASSERT_EQ(test, testSorted);

}

TEST(quickSort, randomInput){
	
	for (int n = 0; n < 100; n++){
		std::vector<int> test2 = generateTestInput(n, n, 0);

		quickSort(test2, 0, n);

		ASSERT_EQ(test2, insertionSort(test2));
	}
	
}

TEST(quickSort, repeatedElements){
	int n = 100;

	std::vector<int> test2 = generateTestInput(n, n/2, 0);

	quickSort(test2, 0, n);

	ASSERT_EQ(test2, insertionSort(test2));
	
}

TEST(quickSort, emptyArray){
	std::vector<int> testv;
	std::vector<int> a = testv;
	quickSort(testv, 0, 0);

	ASSERT_EQ(testv, a);
}

TEST(mergeSort, emptyArray){
	std::vector<int> v;

	ASSERT_EQ(mergeSort<int>(v), v);
}

TEST(mergeSort, randomInput){

	int n = 100;
	std::vector<int> testv = generateTestInput(n, n, 0);
	std::vector<int> testv2 = testv;
	quickSort(testv2, 0, n);

	ASSERT_EQ(mergeSort<int>(testv), testv2);
}