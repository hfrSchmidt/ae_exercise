set terminal png size 1280, 960
set output "wallclockFibonacci.png"
set xlabel "n"
set ylabel "ms"
set yrange [0:0.01]
plot 'clockMeasurements_fromDef.dat' u 1:($4/1000) title 'from_Definition' w l, \
'clockMeasurements_linear.dat' u 1:($4/1000) title 'linear' w l, \
'clockMeasurements_constant.dat' u 1:($4/1000) title 'constant' w l, \
'clockMeasurements_formula.dat' u 1:($4/1000) title 'formula' w l, \
'clockMeasurements_matrix.dat' u 1:($4/1000) title 'matrix' w l, \
'clockMeasurements_lookup.dat' u 1:($4/1000) title 'lookup' w l
