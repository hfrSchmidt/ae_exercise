set terminal png size 1280, 960
set output "cyclesFibonacci.png"
set xlabel "n"
set ylabel "cycles"
set yrange [0:10000]
plot 'cycleMeasurements_fromDef.dat' u 1:4 title 'from_Definition' w l, \
'cycleMeasurements_linear.dat' u 1:4 title 'linear' w l, \
'cycleMeasurements_constant.dat' u 1:4 title 'constant' w l, \
'cycleMeasurements_formula.dat' u 1:4 title 'formula' w l, \
'cycleMeasurements_matrix.dat' u 1:4 title 'matrix' w l, \
'cycleMeasurements_lookup.dat' u 1:4 title 'lookup' w l
