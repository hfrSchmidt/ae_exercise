set terminal png size 1280, 960
set output "sorting_cycles.png"
set xlabel "input size"
set ylabel "cycles"
set yrange [0:10000000]
plot 'cycleMeasurements_insertionSort.dat' u 1:4 title 'insertion sort' w l, \
'cycleMeasurements_mergeSort.dat' u 1:4 title 'merge sort' w l, \
'cycleMeasurements_quickSort.dat' u 1:4 title 'quick sort' w l, \
'cycleMeasurements_cppSort.dat' u 1:4 title 'c++ sort' w l