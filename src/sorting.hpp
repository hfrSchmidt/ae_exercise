#include <vector>

#ifndef SORTING_HPP
#define SORTING_HPP

template <typename T>
extern std::vector<T> insertionSort(std::vector<T> a);

template <typename T>
extern T partition(std::vector<T>& A, T p, T q);

template <typename T>
extern void quickSort(std::vector<T>& A, T p, T q);

template <typename T>
extern std::vector<T> merge(std::vector<T>& inputArr, std::vector<T> l, std::vector<T> r);

template <typename T>
extern std::vector<T> mergeSort(std::vector<T>& inputArr);

template <typename T>
extern std::vector<T> generateTestInput(size_t n, size_t m, T seed);

#endif