set terminal png size 1280, 960
set output "sorting_wallclock.png"
set xlabel "input size"
set ylabel "ms"
set yrange [0:6]
plot 'clockMeasurements_insertionSort.dat' u 1:($4/1000) title 'insertion sort' w l, \
'clockMeasurements_mergeSort.dat' u 1:($4/1000) title 'merge sort' w l, \
'clockMeasurements_quickSort.dat' u 1:($4/1000) title 'quick sort' w l, \
'clockMeasurements_cppSort.dat' u 1:($4/1000) title 'c++ sort' w l