#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <iostream>

#ifndef FIBONACCI_HPP
#define FIBONACCI_HPP

unsigned long fibonacciFromDefinition(unsigned int n);

unsigned long fibonacciLinear(unsigned int n);

unsigned long fibonacciConstant(unsigned int n);

std::vector<std::vector<unsigned long> > matrixMult(std::vector<std::vector<unsigned long> > m, std::vector<std::vector<unsigned long> > m2);

void printm(std::vector<std::vector<unsigned long> > m);

unsigned long fibonacciMatrix(unsigned int n);

unsigned long fibonacciFormula(unsigned int n);

long fibonacciLookup(unsigned int n);

// idea for compile-time calculation taken from 
//"http://milotshala.wordpress.com/2012/03/01/fibonacci-sequence-and-c-template-meta-programming/"
template <int N>
struct Fib
{
	enum { val = (Fib<N-1>::val + Fib<N-2>::val) };
};
template <>
struct Fib<0>
{
	enum {val = 0};
};

template <>
struct Fib<1>
{
	enum {val = 1};
};
template <>
struct Fib<2>
{
	enum {val = 1};
};

#endif